import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../model/User'
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  private usersUrl:string = 'https://my-json-server.typicode.com/Champa123/dbTest/users';
  users:User[];
  
  @Input() userValue:string;
  @Output() userValueChange: EventEmitter<string> = new EventEmitter<string>()
  constructor(
    private userService:UserService
    ) { }

  ngOnInit() {
    this.getUsers();
  }
  
  getUsers(){
    return this.userService.getUsers().subscribe(users => this.users = users);
  } 
}
