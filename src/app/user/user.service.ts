import { Injectable } from '@angular/core';
 
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { catchError } from 'rxjs/operators';
import { User } from '../model/User';
 
@Injectable({
  providedIn: 'root',
})
export class UserService {

    private usersUrl:string = 'https://my-json-server.typicode.com/Champa123/dbTest/users';
 
  constructor( private http:HttpClient) { }
 
  getUsers(){
    return this.http.get<User[]>(this.usersUrl).pipe(
        catchError(this.handleError<User[]>([]))
      );;
  }

  private handleError<T> (result?: T) {
    return (error: any): Observable<T> => {
      console.log(error)
      return of(result as T);
    };
  }
}