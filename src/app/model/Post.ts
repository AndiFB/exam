import { User } from './User'
export class Post {
    id: number;
    title: string;
    user: User;
}