import { Component, OnInit } from '@angular/core';
import {Post} from '../model/Post'
import {PostService} from './posts.service'
import { Observable, of } from 'rxjs';
import { User } from '../model/User';
import { UserService } from '../user/user.service';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts:Post[];
  selectedPost: Post;
  query:string;
  constructor(private postService:PostService, private userService:UserService) { }

  ngOnInit() {
    this.getPosts()
    this.getUsers();
  }
  
  post:Post = new Post();
  checked = false;
  users:User[];
  

  getUsers(){
    return this.userService.getUsers().subscribe(users => this.users = users);
  }

  addPost(post){
    this.postService.addPost(post).subscribe(post => this.posts.push(post));
  }

  toggleAddPost():void {
    this.checked = !this.checked;
  }

  deletePost(deletedPost: Post): void {
    this.posts = this.posts.filter(post => post !== deletedPost);
    this.postService.deletePost(deletedPost).subscribe();
  }

  getPosts(){
    return this.postService.getPosts()
    .subscribe(posts => this.posts = posts);  
  }
}
