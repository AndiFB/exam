import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import { UserComponent } from '../user/user.component';
 
import { PostsComponent }    from './posts.component';
import { PostDetailComponent }  from './post-detail/post-detail.component';
import { SearchPostsPipe } from '../search-posts.pipe';
import { PostsRoutingModule } from './posts-routing.module';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PostsRoutingModule
],
declarations: [
    UserComponent,
    PostsComponent,
    PostDetailComponent,
    SearchPostsPipe
  ]
})
export class PostsModule {}