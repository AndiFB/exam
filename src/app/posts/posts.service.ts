import { Injectable } from '@angular/core';
 
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { catchError, map } from 'rxjs/operators';
import { Post } from '../model/Post';
import { Comment } from '../model/Comment';
 
@Injectable({
  providedIn: 'root',
})
export class PostService {

    private postsUrl:string = 'https://my-json-server.typicode.com/Champa123/dbTest/posts';
    private commentsUrl:string = 'https://my-json-server.typicode.com/Champa123/dbTest/comments';
    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      };
  constructor( private http:HttpClient) { }
 
  getPosts(): Observable<Post[]>{
    return this.http.get<Post[]>(this.postsUrl)
    .pipe(
      catchError(this.handleError<Post[]>([]))
    );
  }

  addPost(post: Post){
    return this.http.post<Post>(this.postsUrl, post, this.httpOptions)
    .pipe(
      catchError(this.handleError<Post>())
    );
  }

  deletePost (post: Post | number): Observable<Post> {
    const id = typeof post === 'number' ? post : post.id;
    const url = `${this.postsUrl}/${id}`;
    return this.http.delete<Post>(url, this.httpOptions).pipe(
      catchError(this.handleError<Post>())
    );
  }

  getPost(id: number | string) {
    return this.getPosts().pipe(
      // (+) before `id` turns the string into a number
      map((heroes: Post[]) => heroes.find(post => post.id === +id))
    );
  }

  getComments(){
    return this.http.get<Comment[]>(this.commentsUrl)
    .pipe(
      catchError(this.handleError<Comment[]>([]))
    );
  }

  private handleError<T> (result?: T) {
    return (error: any): Observable<T> => {
      console.log(error)
      return of(result as T);
    };
  }
}