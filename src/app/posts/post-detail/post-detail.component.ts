import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
 
import { PostService }  from '../posts.service';
import { Post } from 'src/app/model/Post';
import { Comment } from 'src/app/model/Comment';
 
@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {
  post$: Observable<Post>;
  comments:Comment[];
  showComment = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: PostService
  ) {}
 
  ngOnInit() {
    this.post$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service.getPost(params.get('id')))
    );
    this.post$.subscribe(post => this.getComments(post.id));
  }
 
  gotoPosts(post: Post) {
    let postId = post ? post.id : null;
    this.router.navigate(['/posts', { id: postId }]);
  }

  toggleComments(){
    this.showComment = !this.showComment;
    console.log(this.showComment);
  }

  getComments(postId:number){
    this.service.getComments().subscribe(comments => this.comments = comments.filter(comment => comment.postId == postId));
  }
}